# Generate Rockchip style loader binaries
inherit deploy

SRCREV = "f432eb6d8a9d8a9382f6e7f0096fb4ee4672b8e8"

SRC_URI:append = " file://imx8mm-cl-iot-gate-optee_defconfig"
SRC_URI:append = " file://0001-imx8mm-cl-iot-gate-optee-add-dtbo-in-u-boot.itb.patch"
SRC_URI:append = " file://0002-dts-imx8mm-cl-iot-gate-add-tpm2-node.patch"
SRC_URI:append = " file://0003-imx8mm-cl-iot-gate-init-tpm2-when-booting.patch"
SRC_URI:append = " file://0004-imx8mm-cl-iot-gate-apply-dt-overlays.patch"
SRC_URI:append = " file://0005-workaround-disable-verify-time-of-signer-and-signee.patch"
SRC_URI:append = " file://0006-lib-crypto-support-sha384-sha512-in-x509-pkcs7.patch"
SRC_URI:append = " file://0007-drivers-mmc-write-protect-active-boot-area-after-mmc.patch"
SRC_URI:append = " file://0008-drivers-dfu-flash-firmware-to-inactive-boot-area-and.patch"
SRC_URI:append = " file://0009-configs-imx8mm-cl-iot-gate-update-dfu_alt_info-for-f.patch"
SRC_URI:append = " file://0010-net-Add-TCP-protocol.patch"
SRC_URI:append = " file://0011-net-Add-wget-application.patch"
SRC_URI:append = " file://0012-efi_loader-add-sha384-512-on-certificate-revocation.patch"

COMPATIBLE_MACHINE = "imx8mm-cl-iot-gate"

#DEPENDS += " trusted-firmware-a "
DEPENDS += " firmware-imx-8m "

PARALLEL_MAKE = ""

do_compile:prepend() {
	export ATF_LOAD_ADDR=0x920000
	for config in ${UBOOT_MACHINE}; do
		cp ${DEPLOY_DIR_IMAGE}/lpddr*.bin ${B}/${config}/
		cp ${DEPLOY_DIR_IMAGE}/signed*.bin ${B}/${config}/
	done
}

do_compile:append() {
	mkdir -p ${DEPLOYDIR}
	for config in ${UBOOT_MACHINE}; do
	    cp ${B}/${config}/u-boot.bin ${DEPLOYDIR}/u-boot.bin
	done
}

do_install:append() {
	mkdir -p ${D}/usr/lib/u-boot
	for config in ${UBOOT_MACHINE}; do
		cp ${B}/${config}/u-boot.bin ${D}/usr/lib/u-boot
	done
	mkdir -p ${DEPLOYDIR}
	for config in ${UBOOT_MACHINE}; do
	    cp ${B}/${config}/u-boot.bin ${DEPLOYDIR}/u-boot.bin
	done
}

FILES:${PN} += " /usr/lib/u-boot "
SYSROOT_DIRS += " /usr/lib/u-boot "

do_rebuildimage() {
	export BL31="${DEPLOY_DIR_IMAGE}/bl2.bin"
	export FIP="${DEPLOY_DIR_IMAGE}/fip.bin"
	export ATF_LOAD_ADDR=0x920000
       	for config in ${UBOOT_MACHINE}; do
	    cp ${BL31} ${B}/${config}/bl31.bin
	    cp ${FIP} ${B}/${config}/fip.bin
	done
	if [ -n "${UBOOT_CONFIG}" -o -n "${UBOOT_DELTA_CONFIG}" ]
	then
		unset i j k
		for config in ${UBOOT_MACHINE}; do
		    i=$(expr $i + 1);
		for type in ${UBOOT_CONFIG}; do
                    j=$(expr $j + 1);
                    if [ $j -eq $i ]
		    then
			oe_runmake -C ${S} O=${B}/${config} ${UBOOT_MAKE_TARGET}
                    	for binary in ${UBOOT_BINARIES}; do
                            k=$(expr $k + 1);
                            if [ $k -eq $i ]; then
                               cp ${B}/${config}/${binary} ${B}/${config}/${UBOOT_BINARYNAME}-${type}.${UBOOT_SUFFIX}
                            fi
                    	done

		        # Generate the uboot-initial-env
                    	if [ -n "${UBOOT_INITIAL_ENV}" ]; then
                           oe_runmake -C ${S} O=${B}/${config} u-boot-initial-env
                           cp ${B}/${config}/u-boot-initial-env ${B}/${config}/u-boot-initial-env-${type}
                    	fi
			unset k
		    fi
		done
		unset j
	    	done
		unset i
	else
		oe_runmake -C ${S} O=${B} ${UBOOT_MAKE_TARGET}

		# Generate the uboot-initial-env
		if [ -n "${UBOOT_INITIAL_ENV}" ]; then
		      oe_runmake -C ${S} O=${B} u-boot-initial-env
		fi
        fi
}

do_deploy:append() {
	mkdir -p ${DEPLOYDIR}
	for config in ${UBOOT_MACHINE}; do
	    cp -f ${B}/${config}/flash.bin ${DEPLOYDIR}/flash.bin
	done
}

ATF_DEPENDS = " trusted-firmware-a:do_deploy "
do_rebuildimage[depends] .= "${ATF_DEPENDS}"

addtask rebuildimage after do_compile before do_deploy
