# Socionext Synquacer 64-bit machines specific TFA support

COMPATIBLE_MACHINE = "imx8mm-cl-iot-gate"

SRC_URI = "git://git.trustedfirmware.org/TF-A/trusted-firmware-a.git;protocol=https;name=tfa;branch=master"

# TF-A master branch with all Corstone1000 patches merged
SRCREV_tfa = "fdbbd59e978489d3b502bdae63981a5cef8fa405"
PV .= "+git${SRCREV_tfa}"

LIC_FILES_CHKSUM="file://docs/license.rst;md5=b2c740efedc159745b9b31f88ff03dde file://mbedtls/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

TFA_DEBUG = "1"
TFA_UBOOT = "0"
TFA_MBEDTLS = "1"
TFA_BUILD_TARGET = "bl2 bl31 fip"
TFA_INSTALL_TARGET = "fip.bin"

TFA_TARGET_PLATFORM = "imx8mm"

# Enabling Secure-EL1 Payload Dispatcher (SPD)
TFA_SPD = "opteed"

# BL2 loads BL32 (optee). So, optee needs to be built first:
DEPENDS += " optee-os "
DEPENDS:remove = "uboot"

do_compile:prepend() {
	export BL33FILE=`find ${TMPDIR} -name u-boot.bin | tail -1`
}

EXTRA_OEMAKE:append = " \
                        ARCH=aarch64 \
                        PLAT=${TFA_TARGET_PLATFORM} \
			SPD=${TFA_SPD} \
			BL32_BASE=0x7e000000 IMX_BOOT_UART_BASE=0x30880000 \
			NEED_BL32=yes NEED_BL33=yes NEED_BL2=yes \
			USE_TBBR_DEFS=1 GENERATE_COT=1 \
                        TRUSTED_BOARD_BOOT=1 \
			BL2_CFLAGS=-DIMX_FIP_MMAP \
			MEASURED_BOOT=1 TPM_HASH_ALG=sha256 \
                        BL32=${RECIPE_SYSROOT}/lib/firmware/tee-header_v2.bin \
                        BL32_EXTRA1=${RECIPE_SYSROOT}/lib/firmware/tee-pager_v2.bin \
                        BL32_EXTRA2=${RECIPE_SYSROOT}/lib/firmware/tee-pageable_v2.bin \
			BL33=${BL33FILE} \
                        LOG_LEVEL=50 \
			"

do_install:append() {
	mkdir -p ${D}/firmware
	cp ${B}/imx8mm/debug/fip.bin ${D}/firmware
	cp ${B}/imx8mm/debug/bl2.bin ${D}/firmware
}

do_deploy:append() {
	mkdir -p ${DEPLOYDIR}
	cp -f ${B}/imx8mm/debug/fip.bin ${DEPLOYDIR}
	cp -f ${B}/imx8mm/debug/bl2.bin ${DEPLOYDIR}
}

addtask deploy before do_build after do_compile

FILES:${PN} = "/boot /firmware"

do_compile[depends] .= " u-boot:do_install "
