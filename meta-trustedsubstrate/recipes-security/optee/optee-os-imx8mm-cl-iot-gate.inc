SRC_URI = "git://git.trustedfirmware.org/OP-TEE/optee_os.git;protocol=https;branch=master"

#v3.14 sha
SRCREV = "0f8347dcafefa936e0126087043bab4075c983a8"

PV .= "+git${SRCREV}"

DEPENDS += "python3-pycryptodomex-native python3-cryptography-native edk2-firmware"

FILESEXTRAPATHS:prepend := "${THISDIR}/files/optee-os/imx8mm-cl-iot-gate:"

COMPATIBLE_MACHINE = "imx8mm-cl-iot-gate"

OPTEEMACHINE = "imx"
OPTEEOUTPUTMACHINE = "imx"
EXTRA_OEMAKE += " PLATFORM_FLAVOR=mx8mm_cl_iot_gate ARCH=arm"

# Uncoment to enable verbose logs
#EXTRA_OEMAKE += " CFG_TEE_CORE_LOG_LEVEL=4 CFG_EARLY_CONSOLE=1"
EXTRA_OEMAKE += " CFG_TEE_CORE_LOG_LEVEL=2 CFG_TEE_TA_LOG_LEVEL=2"
EXTRA_OEMAKE += " CFG_TEE_CORE_DEBUG=y"
EXTRA_OEMAKE += " CFG_EXTERNAL_DTB_OVERLAY=y"
EXTRA_OEMAKE += " CFG_DT=y"
EXTRA_OEMAKE += " CFG_DT_ADDR=0x52000000"
EXTRA_OEMAKE += " CFG_DEBUG_INFO=y"

EXTRA_OEMAKE += " CFG_ARM64_core=y"

EXTRA_OEMAKE += " HOST_PREFIX=${HOST_PREFIX}"
EXTRA_OEMAKE += " CROSS_COMPILE64=${HOST_PREFIX}"

FILES:${PN} = "/lib/firmware"
SYSROOT_DIRS += "/lib/firmware"

FILES:${PN}-dbg = "/lib/firmware/*.elf"
# Skip QA check for relocations in .text of elf binaries
INSANE_SKIP:${PN}-dbg = "textrel"

# Include StandAloneMM
EXTRA_OEMAKE += ' CFG_RPMB_FS=y CFG_RPMB_FS_DEV_ID=2 CFG_RPMB_WRITE_KEY=y'
EXTRA_OEMAKE += ' CFG_RPMB_TESTKEY=y'
EXTRA_OEMAKE += ' CFG_RPMB_RESET_FAT=n'
EXTRA_OEMAKE += " CFG_STMM_PATH=${DEPLOY_DIR_IMAGE}/uefi.bin "
