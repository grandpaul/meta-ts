#@TYPE: Machine
#@NAME: generic armv8 machine
#@DESCRIPTION: Machine configuration for running a generic armv8

SOC_FAMILY = "imx8mm"

DEFAULTTUNE ?= "cortexa72-cortexa53-crypto"

require conf/machine/include/soc-family.inc
require conf/machine/include/arm/armv8a/tune-cortexa72-cortexa53.inc

MACHINEOVERRIDES =. "imx8mm-cl-iot-gate:mx8m-generic-bsp:"

# Kernel
KERNEL_DEVICETREE = "freescale/imx8mm-evk.dtb"

WKS_FILE_DEPENDS ?= " \
        mtools-native \
        dosfstools-native \
        virtual/bootloader \
        virtual/kernel \
        "
# TF-A
TFA_PLATFORM = "imx8mm"
PREFERRED_VERSION_trusted-firmware-a ?= "2.5%"

# optee
EXTRA_IMAGEDEPENDS += "optee-os"
OPTEE_ARCH = "arm64"
OPTEE_BINARY = "tee-pager_v2.bin"

# nxp-firmware
IMX_MIRROR ?= "https://www.nxp.com/lgfiles/NMG/MAD/YOCTO/"
FSL_MIRROR ?= "${IMX_MIRROR}"
MACHINEOVERRIDES_EXTENDER:imx8mm-cl-iot-gate  = "imx-generic-bsp:imx-mainline-bsp:mx8-generic-bsp:mx8-mainline-bsp:mx8m-generic-bsp:mx8m-mainline-bsp"
ACCEPT_FSL_EULA = "1"
MACHINE_SOCARCH = "imx8mm_cl_iot_gate"
DDR_FIRMWARE_NAME = " \
	lpddr4_pmu_train_1d_imem.bin \
	lpddr4_pmu_train_1d_dmem.bin \
	lpddr4_pmu_train_2d_imem.bin \
	lpddr4_pmu_train_2d_dmem.bin \
"

# u-boot
PREFERRED_VERSION_u-boot ?= "2021.10"
UBOOT_CONFIG ??= "EFI"
UBOOT_CONFIG[EFI] = "imx8mm-cl-iot-gate-optee_defconfig"
UBOOT_ENTRYPOINT ?= "0x40200000"
UBOOT_ARCH = "arm"
include conf/machine/ts-extlinux.conf
UBOOT_EXTLINUX_CONSOLE = "console=ttymxc2,115200 console=tty0"
UBOOT_SUFFIX ?= "bin"
SPL_BINARY ?= "flash.bin"

IMAGE_BOOT_FILES ?= "flash.bin"

SERIAL_CONSOLES ?= "115200;ttymxc2"
SERIAL_CONSOLES_CHECK = "${SERIAL_CONSOLES}"

EXTRA_IMAGEDEPENDS += " u-boot"

IMAGE_FSTYPES += "wic.gz"
WKS_FILE ?= "imx8mm-cl-iot-gate.wks.in"
WKS_FILE_DEPENDS += "u-boot"
